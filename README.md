<p align="center">
  <img width="700" src="images/SA main.png">
</p>

# VueJs-SpringBoot
System Analysis and Design 62/1

# Account Demo
### รหัสสำหรับพนักงาน
- user: `emp2` pass: `emp2p`
- user: `emp3` pass: `emp3p`
### รหัสสำหรับแอดมิน
- user: `admin` pass: `adminp`

# Backend RUN
```
- cd backend
- ./gradlew bootRun
```

# Fronend RUN
```
- install Node & Yarn
- yarn global add @vue/cli@3.1.5
- cd cliend
- npm install
```

# Database H2
- http://localhost:9000/h2-console/ 
- JDBC URL ใส่ค่าเป็น jdbc:h2:mem:testdb

# Demo RECIEVE ORDER page

<p align="center">
  <img width="800" src="images/SA01.png">
  <img width="800" src="images/SA02.png">
  <img width="800" src="images/SA03.png">
</p>




